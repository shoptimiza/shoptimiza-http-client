'use strict';

function parseRemoteTime(input) {
    let t = +input;
    if (Number.isNaN(t)) {
        return 0;
    }
    return t;
}
exports = module.exports = parseRemoteTime;
