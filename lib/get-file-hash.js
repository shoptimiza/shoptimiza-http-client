'use strict';
const fs = require('fs');
const crypto = require('crypto');

function getFileHash(file, callback) {
    fs.createReadStream(file).pipe(crypto.createHash('sha1').setEncoding('base64')).on('finish', function () {
        let hash = this;
        return callback(null, hash.read());
    });
}
exports = module.exports = getFileHash;
