'use strict';
const fs = require('fs');
const retry = require('intenta');
const request = require('request');
const userAgent = require('./user-agent');
const {
    once
} = require('zd-once');
const filteredBackoff = require('./filtered-backoff');
const {
    headerKey,
    signGET,
    unixTime,
} = require('shoptimiza-auth-header');
const parseRemoteTime = require('./parse-remote-time');

function getRequestOptions(apiKey, now, url, sharedSecret) {
    var options = {
        url,
        json: true,
        headers: {
            'User-Agent': userAgent
        }
    };
    options.headers[headerKey] = signGET(apiKey, now, url, sharedSecret);
    return options;
}

function get(apiKey, sharedSecret, url, file, callback) {
    var timeDelta = 0;
    if (!callback) {
        callback = file;
        file = null;
    }
    callback = once(callback);

    function req(callback) {
        var myRequest;
        const now = unixTime() + timeDelta;
        const options = getRequestOptions(apiKey, now, url, sharedSecret);
        var buffer = '';
        var statusCode, headers;

        function onResponse(res) {
            var outputFile;
            statusCode = res.statusCode;
            headers = res.headers;
            if (res.statusCode === 200 && file) {
                outputFile = fs.createWriteStream(file);
                outputFile.on('close', function () {
                    return callback(null, file);
                });
                outputFile.on('error', function (err) {
                    return callback(err);
                });
                return res.pipe(outputFile);
            }
            res.on('data', function (data) {
                buffer += data;
            });
            myRequest.on('complete', onComplete);
        }

        function createError(msg) {
            var err = new Error(msg);
            if (statusCode) {
                err.statusCode = statusCode;
            }
            if (headers) {
                err.headers = headers;
            }
            return err;
        }

        function onComplete() {
            var msg;
            //var reason;
            try {
                msg = JSON.parse(buffer);
            } catch (e) {
                msg = buffer;
            }
            if (statusCode === 200) {
                return callback(null, msg);
            }
            if (statusCode === 404) {
                return callback(createError('not found'));
            }
            if (statusCode === 403 && msg.reason === 'timeout') {
                let remoteTime = parseRemoteTime(msg.time);
                if (remoteTime) {
                    timeDelta = remoteTime - now;
                }
            }
            if (statusCode === 403 && msg.reason.startsWith('invalid')) {
                return callback(createError(msg.reason));
            }
            if (buffer.length === 0) {
                return callback(createError('server returned status code:' + statusCode));
            }
            return callback(createError('Status code: ' + statusCode + '.' + buffer.slice(0, 100)));
        }
        myRequest = request.get(options);
        myRequest.on('error', callback);
        myRequest.on('response', onResponse);
        //.on('complete', onComplete);
    }
    // we want to retry here and not on the caller 
    // because we need to apply the time diff between this machine and remote
    // machine for header auth 
    let retryReq = retry(req, {
        backoff: filteredBackoff,
        report: true
    });
    retryReq(callback);
}
exports = module.exports = {
    getRequestOptions,
    get
};
