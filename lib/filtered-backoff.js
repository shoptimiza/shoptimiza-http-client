'use strict';
const backoff = require('intenta').backoff(100);

function filteredBackoff(attempt, err) {
    switch (err.message) {
    case 'not found':
    case 'invalid apiKey':
    case 'invalid signature':
        return -1;
    default:
        return backoff(attempt);
    }
}
exports = module.exports = filteredBackoff;
