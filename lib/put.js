'use strict';
const fs = require('fs');
const getFileHash = require('./get-file-hash');
const retry = require('intenta');
const request = require('request');
const userAgent = require('./user-agent');
const filteredBackoff = require('./filtered-backoff');
const {
    headerKey,
    signPUT,
    unixTime,
} = require('shoptimiza-auth-header');
const parseRemoteTime = require('./parse-remote-time');

function putRequestOptions(apiKey, now, url, hash, sharedSecret) {
    var options = {
        url,
        json: true,
        headers: {
            'User-Agent': userAgent
        }
    };
    options.headers[headerKey] = signPUT(apiKey, now, url, hash, sharedSecret);
    return options;
}

function pushFileToShoptimizaAPI(apiKey, sharedSecret, url, file, callback) {
    var hash;
    var fn;
    var timeDelta = 0;

    function rq(callback) {
        const now = unixTime() + timeDelta;
        const options = putRequestOptions(apiKey, now, url, hash, sharedSecret);

        function createError(msg, response) {
            var err = new Error(msg);
            if (response) {
                err.statusCode = response.statusCode;
                err.headers = response.headers;
            }
            return err;
        }

        function cb(err, response, body) {
            var reason;
            const statusCode = response.statusCode;
            if (err) {
                return callback(err);
            }
            if (statusCode === 201) {
                return callback(null, true);
            }
            if (statusCode === 404) {
                return callback(createError('not found', response));
            }
            if (statusCode === 403 && body.reason === 'invalid apiKey') {
                return callback(createError(body.reason, response));
            }
            if (statusCode === 403 && body.reason === 'invalid signature') {
                return callback(createError(body.reason, response));
            }
            reason = body && body.reason;
            if (!reason) {
                reason = body;
            }
            if (!reason) {
                reason = 'Error ' + statusCode;
            }
            reason = reason.slice(0,100);
            if (response.statusCode === 403 && response.body && response.body.reason === 'timeout') {
                let remoteTime = parseRemoteTime(response.body.time);
                if (remoteTime) {
                    timeDelta = remoteTime - now;
                }
            }
            return callback(createError(reason, response));
        }
        fs.createReadStream(file).pipe(request.put(options, cb));
    }
    fn = retry(rq, {
        backoff: filteredBackoff,
        report: true
    });
    getFileHash(file, function (err, _hash) {
        hash = _hash;
        fn(callback);
    });
}
exports = module.exports = {
    putRequestOptions,
    putFile: pushFileToShoptimizaAPI
};
