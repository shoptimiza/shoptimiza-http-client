'use strict';
const path = require('path');
const mktemp = require('mktemp');
const fs = require('fs');
const http = require('http');
const userAgent = require('../lib/user-agent');
const express = require('express');
const {
    headerKey,
    signGET,
    unixTime,
    signPOST,
    parseAuthHeader
} = require('shoptimiza-auth-header');
const assert = require('assert');
const {
    getRequestOptions,
    get
} = require('../lib/get');
describe('#getRequestOptions(apiKey, now, url, sharedSecret', function () {
    describe('returned options', function () {
        var apiKey = 'abc',
            now = 123456789,
            url = '/some',
            secret = 'secret';
        var result = getRequestOptions(apiKey, now, url, secret);
        it('should include X-Shoptimiza-Auth header', function () {
            var auth = result.headers['X-Shoptimiza-Auth'].split('.');
            assert.deepEqual(auth[0], apiKey);
            assert.deepEqual(auth[1], now);
            assert(auth[2]);
        });
        it('should include User-Agent header', function () {
            assert.deepEqual(result.headers['User-Agent'], userAgent);
        });
        it('should include url', function () {
            assert.deepEqual(result.url, url);
        });
        it('should set json to true', function () {
            assert.deepEqual(result.json, true);
        });
    });
});
describe('#get(apiKey, sharedSecret, url, callback)', function () {
    describe('expected behavior', function () {
        var headers = [];
        var apiKey = 'abc';
        var secret = 'secret';
        var url = '/api';
        var result;
        before(function (done) {
            var now = unixTime();
            var app = express();
            var i = 0;
            app.use(function (req, res, next) {
                var h = {};
                h.auth = req.get(headerKey);
                h.agent = req.get('user-agent');
                headers.push(h);
                return next();
            });
            app.get('/api', function (req, res) {
                i++;
                switch (i) {
                case 1:
                    {
                        res.status(403);
                        return res.json({
                            reason: 'timeout',
                            time: now + 5000
                        });
                    }
                case 2:
                    {
                        // FORCE ECONNRESET
                        res.socket.destroy();
                        return;
                    }
                case 3:
                    {
                        res.status(500);
                        return res.end();
                    }
                }
                return res.json({
                    mappings: {
                        a: 1
                    }
                });
            });
            app.listen(function (err) {
                var server = this;
                if (err) {
                    return done(err);
                }
                let _url = 'http://localhost:' + server.address().port + url;
                get(apiKey, secret, _url, function () {
                    result = arguments;
                    server.close();
                    return done();
                });
            });
        });
        it('should include User-Agent header', function () {
            assert(headers[0].agent);
            assert(headers[1].agent);
            assert(headers[2].agent);
            assert(headers[3].agent);
            for (let i = 0; i < 4; i++) {
                assert.deepEqual(headers[i].agent, userAgent);
            }
        });
        it('should include X-Shoptimiza-Auth', function () {
            assert(headers[0].auth);
            assert(headers[1].auth);
            assert(headers[2].auth);
            assert(headers[3].auth);
            for (let i = 0; i < 4; i++) {
                assert(parseAuthHeader(headers[i].auth));
            }
        });
        it('should callback  body/mappings', function () {
            var expected = {
                mappings: {
                    a: 1
                }
            };
            assert(!result[0]);
            assert.deepEqual(result[1], expected);
        });
        it('should retry on error', function () {
            assert.deepEqual(headers.length, 4);
        });
        describe('on server "timeout" response', function () {
            it('should fix the time on the X-Shoptimiza-Auth header (now field)', function () {
                var firstHeader = parseAuthHeader(headers[0].auth);
                var secondHeader = parseAuthHeader(headers[1].auth);
                assert(firstHeader.time + 5000 <= secondHeader.time, 'unexpected time difference');
            });
        });
    });
    describe('do not retry on non recoverable errors', function () {
        function test(code, response) {
            describe(`on ${code} ${response}"`, function () {
                var tries = 0;
                var error;
                before(function (done) {
                    var self = this;
                    var app = express();
                    app.get('/', function (req, res) {
                        tries++;
                        res.status(code);
                        return res.json({
                            reason: response
                        });
                    });
                    http.createServer(app).listen(function () {
                        var server = this;
                        self.server = server;
                        const url = `http://localhost:${server.address().port}`;
                        get('12345', 'abc', url, function (err) {
                            error = err;
                            return done();
                        });
                    });
                });
                after(function (done) {
                    if (this.server) {
                        return this.server.close(done);
                    }
                });
                it('should not retry', function () {
                    assert.deepEqual(tries, 1);
                });
                it('should callback `reason`', function () {
                    assert.deepEqual(error.message, response);
                });
                it('error should include statusCode', function () {
                    assert.deepEqual(error.statusCode, code);
                });
                it('error should include headers', function () {
                    assert(error.headers['content-type']);
                });
            });
        }
        test(404, 'not found');
        test(403, 'invalid signature');
        test(403, 'invalid apiKey');
    });
});
describe('#get(apiKey, sharedSecret, url, file, callback)', function () {
    var filePath = path.join(__dirname, './fixtures/file-for-hashing.txt');
    var stat = fs.statSync(filePath);

    function api() {
        before(function createServer(done) {
            var self = this;
            var app = express();
            app.get('/', function (req, res) {
                res.writeHead(200, {
                    'Content-Length': stat.size
                });
                fs.createReadStream(filePath).pipe(res);
                return;
            });
            http.createServer(app).listen(function (err) {
                var server = this;
                self.server = server;
                const url = `http://localhost:${server.address().port}`;
                self.url = url;
                return done(err);
            });
        });
        after(function (done) {
            this.server.close(done);
        });
    }
    describe('happy path', function () {
        before(function (done) {
            var self = this;
            mktemp.createFile('/tmp/XXXXX.txt', function (err, path) {
                self.outputFile = path;
                return done(err);
            });
        });
        after(function (done) {
            fs.unlink(this.outputFile, done);
        });
        api();
        before(function (done) {
            var self = this;
            get('12345', 'abc', this.url, self.outputFile, function (err, file) {
                if (err) {
                    return done(err);
                }
                self.resultFile = file;
                return done();
            });
        });
        it('should write result to a file', function (done) {
            var self = this;
            fs.readFile(self.outputFile, 'utf8', function (err, actual) {
                if (err) {
                    return done(err);
                }
                fs.readFile('./test/fixtures/file-for-hashing.txt', 'utf8', function (err, expected) {
                    if (err) {
                        return done(err);
                    }
                    assert.deepEqual(actual, expected);
                    return done();
                });
            });
        });
        it('should callback same file', function () {
            assert.deepEqual(this.resultFile, this.outputFile);
        });
    });
    describe('file creation error', function () {
        var error;
        before(function () {
            this.outputFile = '/tmp/a/b/c/d/e/f/g';
        });
        api();
        before(function (done) {
            var self = this;
            get('12345', 'abc', self.url, self.outputFile, function (err, file) {
                error = err;
                return done();
            });
        });
        it('should callback same file', function () {
            assert(error, 'did not errored!');
            assert.deepEqual(error.code, 'ENOENT');
        });
    });
});
