'use strict';
const fs = require('fs');
const {
    putFile,
    putRequestOptions
} = require('../lib/put');
const http = require('http');
const path = require('path');
const userAgent = require('../lib/user-agent');
const express = require('express');
const {
    headerKey,
    signPut,
    parseAuthHeader
} = require('shoptimiza-auth-header');
const testFile = path.join(__dirname, './fixtures/file-for-hashing.txt');
const testFileSignature = 'iFAkiKNIlGsmFGR5Nm1nUnHtK8g=';
const assert = require('assert');
describe('#put(apiKey, sharedSecret, url, file, callback)', function () {
    describe('pushing files', function () {
        var tries = 0;
        var body = '';
        var headers = [];
        before(function (done) {
            var app = express();
            app.put('/', function (req, res) {
                tries++;
                headers.push(parseAuthHeader(req.get(headerKey)));
                if (tries < 3) {
                    res.status(500);
                    return res.end();
                }
                req.on('data', function (chunk) {
                    body += chunk;
                });
                req.on('end', function () {
                    res.status(201);
                    return res.end();
                });
            });
            http.createServer(app).listen(function () {
                var server = this;
                const url = `http://localhost:${server.address().port}`;
                putFile('12345', 'secret', url, testFile, function (err, result) {
                    server.close();
                    return done();
                });
            });
        });
        it('should include file signature with the request signature', function () {
            var signatures = headers.map(function (x) {
                return x.bodySignature;
            });
            assert.deepEqual(signatures, [testFileSignature, testFileSignature, testFileSignature]);
        });
        it('should backoff and retry on errors', function () {
            assert.deepEqual(tries, 3);
        });
        it('should push the file', function () {
            fs.readFile(testFile, 'utf8', function (err, data) {
                assert.deepEqual(body, data);
            });
        });
    });
    describe('Time difference between client and server', function () {
        var tries = 0;
        var headers = [];
        before(function (done) {
            var app = express();
            app.put('/', function (req, res) {
                tries++;
                headers.push(parseAuthHeader(req.get(headerKey)));
                if (tries === 1) {
                    res.status(403);
                    return res.json({
                        reason: 'timeout',
                        time: 5000 // our unix time
                    });
                }
                if (tries < 4) {
                    res.status(500);
                    return res.end();
                }
                res.status(201);
                return res.end();
            });
            http.createServer(app).listen(function () {
                var server = this;
                var url = `http://localhost:${server.address().port}`;
                putFile('12345', 'secret', url, testFile, function (err) {
                    server.close();
                    return done(err);
                });
            });
        });
        it('should fix the time difference', function () {
            var times = headers.map(function (x) {
                return x.time;
            });
            assert(times[0] / 5000 > 100);
            assert(times[1] / 5000 < 2);
        });
        it('should carry time difference', function () {
            var times = headers.map(function (x) {
                return x.time;
            });
            assert(times[1] - 5000 <= 2);
            assert(times[2] - 5000 <= 2);
            assert(times[3] - 5000 <= 2);
        });
    });
    describe('do not retry on non recoverable errors', function () {
        function test(code, response) {
            describe(`on ${code} ${response}"`, function () {
                var tries = 0;
                var error;
                before(function (done) {
                    var app = express();
                    app.put('/', function (req, res) {
                        tries++;
                        res.status(code);
                        return res.json({
                            reason: response
                        });
                    });
                    http.createServer(app).listen(function () {
                        var server = this;
                        var url = `http://localhost:${server.address().port}`;
                        putFile('12345', 'abc', url, testFile, function (err) {
                            assert.deepEqual(tries, 1);
                            error = err;
                            server.close();
                            return done();
                        });
                    });
                });
                it('should not retry', function () {
                    assert.deepEqual(tries, 1);
                });
                it('should callback `reason`', function () {
                    assert.deepEqual(error.message, response);
                });
                it('error should include statusCode', function () {
                    assert.deepEqual(error.statusCode, code);
                });
                it('error should include headers', function () {
                    assert(error.headers['content-type']);
                });
            });
        }
        test(404, 'not found');
        test(403, 'invalid signature');
        test(403, 'invalid apiKey');
    });
});
