'use strict';
const assert = require('assert');
const getFileHash = require('../lib/get-file-hash');
const path= require('path');
describe('#getFileHash(file, callback)', function () {
    it('should get the digest', function (done) {
        // we got the original hash by 
        // cat test/fixtures/file-for-hashing.txt | sha1sum | cut -f1 -d\  | xxd -r -p | base64
        const expected = 'iFAkiKNIlGsmFGR5Nm1nUnHtK8g=';
        const file = path.join(__dirname, './fixtures/file-for-hashing.txt');
        getFileHash(file, function (err, hash) {
            if (err) {
                return done(err);
            }
            assert.deepEqual(hash, expected);
            return done();
        });
    });
});
