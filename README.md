[![pipeline status](https://gitlab.com/shoptimiza/shoptimiza-http-client/badges/master/pipeline.svg)](https://gitlab.com/shoptimiza/shoptimiza-http-client/commits/master)
[![coverage report](https://gitlab.com/shoptimiza/shoptimiza-http-client/badges/master/coverage.svg)](https://gitlab.com/shoptimiza/shoptimiza-http-client/commits/master)

# Shoptimiza HTTP client

## Install
```
npm install shoptimiza-http-client
```

## Usage 
```
const {get} = require('shoptimiza-http-client');

get(apiKey, sharedSecret, url, function(err, result){
	if(err){
		...
	}
});
```

`get` Will retry (up to 5 times) in case of error due to a server error or time difference between client and server.  
An exponential backoff strategy will be applied on server errors.

