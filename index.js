'use strict';
const {
    get
} = require('./lib/get');
const {
    putFile
} = require('./lib/put');


exports = module.exports = {
    get,
    putFile
};
